
const ChargeSheetModel = require("../models/charge_Sheet")
const BillingModel = require("../models/billing")

function generateChargeSheetId() {
    // Implement logic to generate a unique ID
    return Math.floor(Math.random() * 1000000) + 1; // Example placeholder
}

const generateChargeSheet =  (async (req, res) => {
    const { appointment_date, charge, patient_id, doctor_id,bill_id=null } = req.body;
    const charge_id = generateChargeSheetId();
    const newBilling = new ChargeSheetModel({
        charge_id,
        appointment_date,
        patient_id,
        doctor_id,
        bill_id,
        charge
      
        
       
    });
    const response= await newBilling.save();
    res.status(200).json(response)
})



const updateBillId=(async(req,res)=>{
    try{
    const { chargeId } = req.params ;
    console.log(chargeId)
    const chargeIdNum=Number(chargeId)
    
    const {bill_id}=req.body;
    console.log(bill_id)
    
    
    const chargeSheetRecord = await ChargeSheetModel.findOne({charge_id:chargeIdNum})
    
    if (!chargeSheetRecord) {
        return res.status(404).json({ error: 'Charge sheet record not found' });
      }
      const updatedChargeSheet = await ChargeSheetModel.findByIdAndUpdate(
        chargeSheetRecord._id,
        { $set: { bill_id: bill_id } },
        { new: true }
      );

      if (bill_id) {
        const updatedBillingRecord = await BillingModel.findOneAndUpdate(
            { bill_id: updatedChargeSheet.bill_id },
          { $set: { charge_id: chargeSheetRecord.charge_id } },
          { new: true }
        );
  
    //     // Check if the billing record was updated successfully
        if (!updatedBillingRecord) {
          return res.status(500).json({ error: 'Failed to update billing record' });
        }
      }
  
      // Respond with success
      res.status(200).json({
        message: 'Bill ID updated successfully',
        updatedChargeSheet: updatedChargeSheet,
      });
    }
    catch(error){
        console.log(error)
        res.status(200).json({
            message: error,
          
          });
    }


})



module.exports={
    generateChargeSheet,
    updateBillId
}