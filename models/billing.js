const mongoose = require("mongoose")

const BillingSchema = new mongoose.Schema({

    bill_id: {
        type: Number, 
        required: true,
        unique: true
      },
      payment_status:{
        type:String,
        required:true
      },
      payment_date: {
        type: Date,
        required: true
      },
      amount: { 
        type: mongoose.Decimal128,
        required: true,
        min: 0,
        max: 99999.99
      },
      patient_id: {
        type: Number,
        required: true
      },
      doctor_id: {
        type: Number,
        required: true
      },
      charge_id: {
        type: Number,
        
        
      },
    
})

const BillingModel= mongoose.model('Billings',BillingSchema)
module.exports=BillingModel


